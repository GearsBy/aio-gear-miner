package AGM;

import AGM.core.*;
import AGM.data.Location;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

import java.awt.*;
import java.util.ArrayList;

@ScriptMeta(developer = "GearsBy", desc = "Progressive Miner, you set which ores and location then when you hit that level it'll move on, if banking will check bank for better pickaxe ", name = "AIO Gear Miner", category = ScriptCategory.MINING)
public class AIOGearMiner extends TaskScript implements RenderListener {

    public Area bankArea, mineArea;

    public static ArrayList<Location> progession = new ArrayList<Location>();
    public static ArrayList<Location> progessionALL = new ArrayList<Location>();
    public static ArrayList<Long> hopList = new ArrayList<Long>();

    public static boolean f2p = true;
    public static int worldHopPlayers, maxHops;
    public static long worldHopTimeOut;
    public static boolean misClick, GUIDone= false;

    public static int dropPattern, fatigueRate, speed;

    public static Settings settings = null;
    public static final Task[] TASKS = { new Progressor(), new WorldHop(), new MineRocks(), new Banking(), new Drop(), new Walking()};
    private int i;
    public static Location currentLoc;

    public static SceneObject currentRock;

    public static int startXp;
    public static long startTime;

    @Override
    public void onStart(){

        Log.info("Welcome");
        startXp = Skills.getExperience(Skill.MINING);
        startTime = System.currentTimeMillis();

        new GUI().setVisible(true);

        submit(TASKS);

    }

    public final String formatTime(final long ms) {
        long s = ms / 1000, m = s / 60, h = m / 60;
        s %= 60;
        m %= 60;
        h %= 24;
        return String.format("%02d:%02d:%02d", h, m, s);
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        int currentXp = Skills.getExperience(Skill.MINING);
        float runTime = System.currentTimeMillis() - startTime;
        Graphics g = renderEvent.getSource();
        int xpGained = currentXp - startXp;
        g.setColor(Color.BLACK);
        g.fillRect(3, 304, 516, 36);
        g.setColor(Color.WHITE);
        g.drawString("Experience Gained: " + xpGained, 11, 329);
        g.drawString("Time: " + formatTime((long)runTime), 219, 329);
        g.drawString("GearsBy AIO Miner", 411, 329);
    }



}