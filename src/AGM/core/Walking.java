package AGM.core;

import AGM.AIOGearMiner;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class Walking extends Task {

    @Override
    public boolean validate(){

        if(AIOGearMiner.settings == null){
            return false;
        }

        if(Players.getLocal().isAnimating() && Players.getLocal().isMoving()){
            return false;
        }
        //at bank need to go ore, or at ore and banking with full inv, or lost

        if(AIOGearMiner.currentLoc.getBankArea().contains(Players.getLocal()) && (Inventory.getCount() < 28)){
            return true;
        } else if ((AIOGearMiner.currentLoc.getOreArea().contains(Players.getLocal())) && (Inventory.getCount() == 28)){
            return true;
        } else if (!AIOGearMiner.currentLoc.getBankArea().contains(Players.getLocal()) && !AIOGearMiner.currentLoc.getOreArea().contains(Players.getLocal())){
            return true;
        }

        return false;
    }

    @Override
    public int execute(){

        walk(Inventory.isFull() ? 1 : 0);

        return 300;

    }


    public boolean atKnownLoc(){
        return false;
    }

    public void walk(int where){

        if(!Movement.isRunEnabled() && (Movement.getRunEnergy() > AIOGearMiner.settings.wait(25,55))){
            Movement.toggleRun(true);
        }

        while(Players.getLocal().isMoving()){
            Time.sleep(AIOGearMiner.settings.wait(200,650));
        }

        int randA = AIOGearMiner.settings.wait(-2,2);
        int randB = AIOGearMiner.settings.wait(-2,2);
        int x,y;
        switch(where) {

            case 0:
                Log.info("Walking to Ore Area");
                x = AIOGearMiner.currentLoc.getOreArea().getCenter().getX();
                y = AIOGearMiner.currentLoc.getOreArea().getCenter().getY();
                Movement.walkTo(new Position(x+randA, y+randB));
                break;
            case 1:
                Log.info("Walking to Bank");
                x = AIOGearMiner.currentLoc.getBankArea().getCenter().getX();
                y = AIOGearMiner.currentLoc.getBankArea().getCenter().getY();
                Movement.walkTo(new Position(x+randA, y+randB));
                break;
        }
        Time.sleep(AIOGearMiner.settings.wait(200,650));

    }
}
