package AGM.core;

import AGM.AIOGearMiner;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import javax.swing.text.Position;
import java.util.stream.IntStream;

public class MineRocks extends Task {

    @Override
    public boolean validate(){

        if(AIOGearMiner.settings == null){
            return false;
        }

        if(Players.getLocal().isMoving()){
            return false;
        }

        if(Players.getLocal().isAnimating()){
            SceneObject rocks = SceneObjects.getNearest(rock -> rock.getPosition().equals(AIOGearMiner.currentRock.getPosition()));

            if((rocks.getId() == 7468) || (rocks.getId() == 7469)){
            } else {
                return false;
            }
        }
        if(!AIOGearMiner.currentLoc.getOreArea().contains(Players.getLocal()) ){
            Log.info("Not at area");
            return false;
        } else if (Inventory.getCount() == 28){
            return false;

        }
        int count = Inventory.getCount(item -> item.getName().endsWith("ore"));
        if (AIOGearMiner.settings.dropPatterenM1D1() && (count > 0) ){
            return false;
        } else if (AIOGearMiner.settings.dropPatterenM2D2() && (count > 1)){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int execute(){


        if(AIOGearMiner.currentLoc.standStill()){
            if(!(AIOGearMiner.currentLoc.standStillSpot().equals(Players.getLocal().getPosition()))){
                Movement.walkTo(AIOGearMiner.currentLoc.standStillSpot());
                Time.sleep(100);
                while(Players.getLocal().isMoving()){
                    Time.sleep(AIOGearMiner.settings.wait(100,300));
                }
            }
        }

        SceneObject rock;

        if(AIOGearMiner.currentLoc.standStill()){
            rock = SceneObjects.getNearest(rocks -> ((rocks.getName().equals("Rocks")  &&
                    IntStream.of(AIOGearMiner.currentLoc.getOre().getIds()).anyMatch(x -> x == rocks.getId())) && (
                    ((Math.abs(rocks.getPosition().getX()-Players.getLocal().getPosition().getX()) == 1) &&
                            (Math.abs(rocks.getPosition().getY()-Players.getLocal().getPosition().getY()) == 0)) ||
                            ((Math.abs(rocks.getPosition().getX()-Players.getLocal().getPosition().getX()) == 0) &&
                            (Math.abs(rocks.getPosition().getY()-Players.getLocal().getPosition().getY()) == 1)))));
        } else {
            rock = SceneObjects.getNearest(rocks -> ((rocks.getName().equals("Rocks")  &&
                    IntStream.of(AIOGearMiner.currentLoc.getOre().getIds()).anyMatch(x -> x == rocks.getId()))));
        }


        if(rock != null){
            AIOGearMiner.currentRock = rock;
            rock.interact("Mine");

            long timer = System.currentTimeMillis();


            //wait for a bit to not spam click/drop
            Time.sleep(AIOGearMiner.settings.wait(200,600));


        } else {
            return AIOGearMiner.settings.wait(300,750);
        }

        return AIOGearMiner.settings.wait(100,300);

    }

}
