package AGM.core;

import AGM.AIOGearMiner;
import AGM.data.DropType;
import AGM.data.Location;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GUI extends JFrame {

    private JComboBox loc1, loc2, loc3, loc4, loc5, dropType;
    private JButton startButton;
    private JSpinner fatigueSpinner, speedSpinner, hopWhen, maxHops;
    private SpinnerNumberModel fatModel, speedModel, hopWhenM, maxHopsM;
    private JCheckBox misClick;

    public GUI() {

        super("AIO Gear Miner - Config");

        fatModel = new SpinnerNumberModel(0,0,10,1);
        speedModel = new SpinnerNumberModel(0,0,2,1);
        hopWhenM = new SpinnerNumberModel(4,2,40,1);
        maxHopsM = new SpinnerNumberModel(4,0,40,1);


        setLayout(new GridLayout(12,2));

        startButton = new JButton("Start");
        loc1 = new JComboBox(Location.values());
        loc2 = new JComboBox(Location.values());
        loc3 = new JComboBox(Location.values());
        loc4 = new JComboBox(Location.values());
        loc5 = new JComboBox(Location.values());
        dropType = new JComboBox(DropType.values());
        fatigueSpinner = new JSpinner(fatModel);
        speedSpinner = new JSpinner(speedModel);
        hopWhen = new JSpinner(hopWhenM);
        maxHops = new JSpinner(maxHopsM);
        misClick = new JCheckBox();
        startButton = new JButton("Start");

        add(new JLabel("Location 1"));
        add(loc1);
        add(new JLabel("Location 2"));
        add(loc2);
        add(new JLabel("Location 3"));
        add(loc3);
        add(new JLabel("Location 4"));
        add(loc4);
        add(new JLabel("Location 5"));
        add(loc5);
        add(new JLabel("Drop Type"));
        add(dropType);
        add(new JLabel("Fatigue rate (0 = None) "));
        add(fatigueSpinner);
        add(new JLabel("Speed (0/1/2 = Fast/Medium/Slow) "));
        add(speedSpinner);
        add(new JLabel("Misclick?"));
        add(misClick);
        add(new JLabel("World Hop when how many people"));
        add(hopWhen);
        add(new JLabel("Max number of world hops per 15 min?"));
        add(maxHops);
        add(new JLabel("Start"));
        add(startButton);

        setDefaultCloseOperation(HIDE_ON_CLOSE);

        pack();

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(((Location) loc1.getSelectedItem()).getOre() != null){
                    AIOGearMiner.progession.add((Location) loc1.getSelectedItem());
                }
                if(((Location) loc2.getSelectedItem()).getOre() != null){
                    AIOGearMiner.progession.add((Location) loc2.getSelectedItem());
                }
                if(((Location) loc3.getSelectedItem()).getOre() != null){
                    AIOGearMiner.progession.add((Location) loc3.getSelectedItem());
                }
                if(((Location) loc4.getSelectedItem()).getOre() != null){
                    AIOGearMiner.progession.add((Location) loc4.getSelectedItem());
                }
                if(((Location) loc5.getSelectedItem()).getOre() != null){
                    AIOGearMiner.progession.add((Location) loc5.getSelectedItem());
                }

                AIOGearMiner.dropPattern = ((DropType) dropType.getSelectedItem()).dropType();
                AIOGearMiner.fatigueRate = (int) fatigueSpinner.getValue();
                AIOGearMiner.misClick = misClick.isSelected();

                AIOGearMiner.maxHops = (int) maxHops.getValue();
                AIOGearMiner.worldHopPlayers = (int) hopWhen.getValue();

                AIOGearMiner.GUIDone = true;

                setVisible(false);
            }
        });
    }


}
