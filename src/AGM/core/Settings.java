package AGM.core;

import java.util.Random;

public class Settings {

    private long startTime;
    private int speed, fatigueRate, dropPattern;

    private boolean misClick;

    public Settings(boolean fatigue, int fatigueRate, int speed, int dropPattern, boolean misClick){

        this.startTime = System.currentTimeMillis();
        this.dropPattern = dropPattern;
        this.misClick = misClick;
        if (fatigue){
            this.fatigueRate = fatigueRate;
        } else {
            this.fatigueRate = 0;
        }

        if (fatigueRate > 10){
            this.fatigueRate = 10;
        } else {
            this.fatigueRate = fatigueRate;
        }

        switch(speed){
            case 0:
                break;
            case 1:
                this.speed = wait(250,1300);
                break;
            case 2:
                this.speed = wait(700,1600);
                break;

        }

    }

    public boolean banking(){
        return (dropPattern == 0);
    }

    public boolean dropPatterenM1D1(){
        return (dropPattern == 1);
    }

    public boolean dropPatterenM2D2(){
        return (dropPattern == 2);
    }

    public boolean dropFUll(){
        return (dropPattern == 3);
    }

    public int wait(int min, int max){

        min += speed;
        max += speed;

        long runTime = System.currentTimeMillis() - startTime;
        int runTimeMins = (int) (runTime/(60*1000));
        int fatigue = fatigueRate * runTimeMins;

        min+= fatigue;
        max+= fatigue;



        Random r = new Random();
        float sd = ((max - min)/6);
        int res = (int) ((r.nextGaussian()*sd) + ((max-min)/2));
        if (res < min){
            return min;
        } else {
            return res;
        }
    }

    public boolean misClick(){
        return misClick;
    }



}
