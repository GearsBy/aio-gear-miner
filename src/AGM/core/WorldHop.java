package AGM.core;

import AGM.AIOGearMiner;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class WorldHop extends Task {

    @Override
    public boolean validate(){

        if(AIOGearMiner.settings == null){
            return false;
        }

        if(AIOGearMiner.hopList.size() != 0){
            if((AIOGearMiner.hopList.get(0).longValue() - System.currentTimeMillis()) > AIOGearMiner.worldHopTimeOut){
               AIOGearMiner.hopList.remove(0);
               AIOGearMiner.worldHopTimeOut = AIOGearMiner.settings.wait(10,20) * 60*1000;
            }
        }

        if(AIOGearMiner.hopList.size() >= AIOGearMiner.maxHops){
            return false;
        }

        if ((AIOGearMiner.currentLoc.getOreArea().contains(Players.getLocal())) &&
                Players.getLoaded(player -> player.distance(Players.getLocal().getPosition()) < 4).length > AIOGearMiner.worldHopPlayers){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int execute(){

        Log.info("Hopping");

        AIOGearMiner.hopList.add((Long) System.currentTimeMillis());

        if (AIOGearMiner.f2p){
            WorldHopper.randomHopInF2p();
        } else {
            WorldHopper.randomHopInP2p();
        }

        return AIOGearMiner.settings.wait(6000,20000);
    }

}
