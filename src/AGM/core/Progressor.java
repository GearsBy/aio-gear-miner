package AGM.core;

import AGM.AIOGearMiner;
import AGM.data.Location;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.Random;

public class Progressor extends Task {

    @Override
    public boolean validate(){

        if(AIOGearMiner.currentLoc == null){
            return true;
        } else {
            Random r = new Random();
            return r.nextInt(20) == 5; //Only need to check if we move on every few loops
        }
    }

    public int execute(){


        if((AIOGearMiner.settings == null) && AIOGearMiner.GUIDone){

            boolean fatigue = false;

            if(AIOGearMiner.fatigueRate != 0){
                fatigue = true;
            }

            AIOGearMiner.settings = new Settings(fatigue, AIOGearMiner.fatigueRate,
                    AIOGearMiner.speed, AIOGearMiner.dropPattern, AIOGearMiner.misClick);

            AIOGearMiner.progessionALL = AIOGearMiner.progession;

            AIOGearMiner.hopList.add(System.currentTimeMillis());

            AIOGearMiner.worldHopPlayers = 6;
            AIOGearMiner.maxHops = 6;
            AIOGearMiner.worldHopTimeOut = AIOGearMiner.settings.wait(14,15) * 60*1000;


        } else if (AIOGearMiner.settings == null){
            return 500;
        }

        if(AIOGearMiner.currentLoc == null){
            Log.info("CurrentLoc null, looking to progession");
            if(AIOGearMiner.progession == null){
                //exit no stuff to do sad
                Log.info("Null :/");
            } else {
                Log.info("Updating Current Loc");
                AIOGearMiner.currentLoc = AIOGearMiner.progession.get(0);
                if(AIOGearMiner.currentLoc.getOre().getLevel() > Skills.getCurrentLevel(Skill.MINING)){
                    // exit, too high skill level ERROR
                    Log.info("Level too high");
                }
            }
        }

        //Check if more locs and if so level reqs

        if(AIOGearMiner.progession.size() > 1){
            if(AIOGearMiner.progession.get(1).getOre().getLevel() <= Skills.getCurrentLevel(Skill.MINING)){
                Log.info("Moving to next Loc");
                AIOGearMiner.progession.remove(0);
                AIOGearMiner.currentLoc = AIOGearMiner.progession.get(0);
            }
        }

        return 0;
    }
}
