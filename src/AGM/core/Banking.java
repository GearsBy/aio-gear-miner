package AGM.core;

import AGM.AIOGearMiner;
import AGM.data.Pickaxe;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Equipment;
import org.rspeer.runetek.api.component.tab.EquipmentSlot;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Banking extends Task {

    private final static Predicate<Item> AXE_PREDICATE = item -> item.getName().contains("axe");

    @Override
    public boolean validate(){
        if(AIOGearMiner.settings == null){
            return false;
        }
        return AIOGearMiner.currentLoc.getBankArea().contains(Players.getLocal()) && (Inventory.isFull() );
    }

    @Override
    public int execute() {
        Log.info("Banking");

        Pickaxe currentPick = null;
        Pickaxe bestPick = null;

        boolean pickInHand = false;
        boolean changePick = false;

        if(EquipmentSlot.MAINHAND.getItem() != null ){
            if(EquipmentSlot.MAINHAND.getItem().getName().contains("axe")){
                currentPick = getPick(EquipmentSlot.MAINHAND.getItem().getName());
                pickInHand = true;
            }
        }

        if(Inventory.contains(item -> item.getName().contains("axe"))){
            currentPick = getPick(Inventory.getItems(item -> item.getName().contains("axe"))[0].getName());
            pickInHand = false;
        }

        int val = -1;

        if (currentPick != null){
            val = currentPick.getValue();
        }

            if (!(Bank.isOpen())) {
                Bank.open();
                Time.sleep(AIOGearMiner.settings.wait(800, 2500));
            }


            if (Bank.isOpen()) {

                if(val != -1){
                    final Pickaxe cPick = currentPick;
                    Item[] betterPick =  Bank.getItems(item -> item.getName().contains("axe") && getPick(item.getName()).canUse()
                    && cPick.isThisBetter(getPick(item.getName())));
                    if(betterPick != null){
                        for(Item i: betterPick){
                            if (bestPick == null){
                                bestPick = getPick(i.getName());
                            } else {
                                if(bestPick.isThisBetter(getPick(i.getName()))){
                                    bestPick = getPick(i.getName());
                                }
                            }

                        }
                    }
                }



                Bank.depositAllExcept(AXE_PREDICATE);
                Time.sleep(AIOGearMiner.settings.wait(800, 2200));

                if(bestPick != null){
                    if(!pickInHand){
                        Bank.depositAll(item -> item.getName().contains("axe"));
                        Time.sleep(AIOGearMiner.settings.wait(800, 2200));
                        Bank.withdraw(bestPick.getName(),1);
                        Time.sleep(AIOGearMiner.settings.wait(800, 2200));
                    } else {
                        changePick = true;
                    }
                }

               Bank.close();
                Time.sleep(AIOGearMiner.settings.wait(800, 2200));
            } else {
                //Error
            }

            if(changePick){
                EquipmentSlot.MAINHAND.unequip();
                Time.sleep(AIOGearMiner.settings.wait(800, 2200));
                Bank.open();
                Time.sleep(AIOGearMiner.settings.wait(800, 2200));
                if(Bank.isOpen()){
                    Bank.depositAll(item -> item.getName().contains("axe"));
                    Time.sleep(AIOGearMiner.settings.wait(800, 2200));
                    Bank.withdraw(bestPick.getName(),1);
                    Time.sleep(AIOGearMiner.settings.wait(800,2200));
                    Bank.close();
                    Time.sleep(AIOGearMiner.settings.wait(800,2200));
                }

            }


            if(Inventory.contains(item -> item.getName().contains("axe"))){
                Item pick = Inventory.getItems(item -> item.getName().contains("axe"))[0];
                if(getPick(pick.getName()).canHold()){
                    pick.interact("Wield");
                    Time.sleep(AIOGearMiner.settings.wait(800,2200));
                }
            }

            return 300;

        }


        private Pickaxe getPick(String name){

        switch(name){
            case "Bronze pickaxe":
                return  Pickaxe.BRONZE;
            case "Iron pickaxe":
                return Pickaxe.IRON;
            case "Steel pickaxe":
                return Pickaxe.STEEL;
            case "Black pickaxe":
                return Pickaxe.BLACK;
            case "Adamant pickaxe":
                return Pickaxe.ADAMANT;
            case "Mithril pickaxe":
                return Pickaxe.MITHRIL;
            case "Rune pickaxe":
                return Pickaxe.RUNE;
        }
        return Pickaxe.BRONZE;
        }




}
