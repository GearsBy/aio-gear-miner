package AGM.core;

import AGM.AIOGearMiner;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import java.util.*;
import java.util.function.Predicate;

public class Drop extends Task {

    private final static Predicate<Item> ORE_PREDICATE = item -> item.getName().contains("ore") || item.getName().equals("Coal");

    @Override
    public boolean validate(){

        if(AIOGearMiner.settings == null){
            return false;
        }

        if(Players.getLocal().isAnimating() && Players.getLocal().isMoving()){
            return false;
        }

        int invCount = Inventory.getCount(ORE_PREDICATE);

        if ((invCount == 28) && !AIOGearMiner.settings.banking()){
            return true;
        } else if ((invCount > 0) && AIOGearMiner.settings.dropPatterenM1D1()){
                return true;
        } else if ((invCount > 1) && AIOGearMiner.settings.dropPatterenM2D2()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int execute(){
        Log.info("---Droping---");

        int count = Inventory.getCount();
        long timer;

        int randHit = 0,randSeed = 0;
        Random r = new Random();

        if(AIOGearMiner.settings.misClick()){
            randSeed = AIOGearMiner.settings.wait(25, 300);
            randHit = r.nextInt(randSeed);
        }


        //sleep as Mine exits quickly so can look for next rock
        Time.sleep(AIOGearMiner.settings.wait(650,1900));

        //simple handle for M1D1/M2D2
        if(Inventory.getCount()<3){
            Log.info("Boring drop");
            for(Item item: Inventory.getItems()){
                count = Inventory.getCount();
                if(item.getName().endsWith("ore") || item.getName().equals("Coal")){
                    if((randSeed != 0) && (r.nextInt(randSeed) == randHit)) {
                        switch (r.nextInt(3)){
                            case 0:
                                item.interact("Cancel");
                                break;
                            case 1:
                                item.interact("Examine");
                                break;
                            case 2:
                                item.interact("Use");
                                break;
                        }
                    } else {
                        item.interact("Drop");
                    }
                    //Wait for item to dissappear (2 second time out)
                    timer = System.currentTimeMillis();
                    while( ((System.currentTimeMillis() - timer) < 2000) && (Inventory.getCount() == count)){
                        Time.sleep(250);
                    }
                    //Sleep for a bit longer, we're not a bot
                    Time.sleep(AIOGearMiner.settings.wait(200,600));
                }
            }
        } else {
            //fancy drop patterns for full inv
            Log.info("Fancy drop");
            Item[] items = Inventory.getItems();
            List<Item> dropItems= new ArrayList<Item>();
            switch(r.nextInt(3)){
                case 0:
                    Log.info("Straight drop");
                    dropItems = Arrays.asList(items);
                    break;
                case 1:
                    Log.info("Reverse drop");
                    Collections.reverse(Arrays.asList(items));
                    dropItems = Arrays.asList(items);
                    break;
                case 2:
                    Log.info("Vertical drop");
                    for(int i = 0; i<4; i++)
                        for(int j = 0; j< 7; j++){
                            if(!((items.length-1) > (i*j))){
                                dropItems.add(items[(i*j)]);
                            }
                        }
                    break;

            }

            for(Item i: dropItems){
                count = Inventory.getCount();
                if(i.getName().endsWith("ore") || i.getName().equals("Coal")){
                    i.interact("Drop");
                    //Wait for item to dissappear (2 second time out)
                    timer = System.currentTimeMillis();
                    while( ((System.currentTimeMillis() - timer) < 2000) && (Inventory.getCount() == count)){
                        Time.sleep(250);
                    }
                    //Sleep for a bit longer, we're not a bot
                    Time.sleep(AIOGearMiner.settings.wait(200,600));
                }

            }

        }

        //check to see for see misclick

        if(Inventory.contains(ORE_PREDICATE)){
            Item[] items = Inventory.getItems(ORE_PREDICATE);
            for(Item i :items){
                i.interact("Drop");
                //Wait for item to dissappear (2 second time out)
                timer = System.currentTimeMillis();
                while( ((System.currentTimeMillis() - timer) < 2000) && (Inventory.getCount() == count)){
                    Time.sleep(250);
                }
                //Sleep for a bit longer, we're not a bot
                Time.sleep(AIOGearMiner.settings.wait(200,600));
            }
        }



        Log.info("---Finished Dropping---");
        return AIOGearMiner.settings.wait(200,500);

    }

    private void misClick(Item i){

    }


}
