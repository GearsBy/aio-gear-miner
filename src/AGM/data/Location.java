package AGM.data;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public enum Location {


    NO_MORE(null,null,null,null),
    LUMBRIDGE_COPPER_TIN(Area.rectangular(3205, 3213, 3213, 3223, 2),  Area.rectangular(3221, 3143, 3232, 3149), Ores.COPPER_TIN, new Position(0,0)),
    VAROCK_EAST_COPPER_TIN(Area.rectangular(3250, 3424, 3257, 3418),Area.rectangular(3280, 3360, 3291, 3371),Ores.COPPER_TIN, new Position(0,0)),
    VAROCK_EAST_IRON(Area.rectangular(3250, 3424, 3257, 3418),Area.rectangular(3280, 3360, 3291, 3371),Ores.IRON, new Position(3286,3368)),
    RIMMINGTON_COPPER_TIN(Area.rectangular(3009, 3354, 3016, 3357),Area.rectangular(2966, 3228, 2989, 3248),Ores.COPPER_TIN, new Position(0,0)),
    RIMMINGTON_IRON(Area.rectangular(3009, 3354, 3016, 3357),Area.rectangular(2966, 3228, 2989, 3248),Ores.IRON, new Position(2981,3234)),
    RIMMINGTON_GOLD(Area.rectangular(3009, 3354, 3016, 3357),Area.rectangular(2966, 3228, 2989, 3248),Ores.GOLD, new Position(0,0)),
    BARBARIAN_VILLAGE_COAL(Area.rectangular(3092, 3488, 3097, 3498), Area.rectangular(3079, 3417, 3088, 3426), Ores.COAL, new Position(0,0));

    private final Area bankArea, oreArea;
    private final Ores ores;
    private final Position mineSpot;

    Location(final Area bankArea, final Area oreArea, final Ores ore, final Position mineSpot){
        this.bankArea = bankArea;
        this.oreArea = oreArea;
        this.ores = ore;
        this.mineSpot = mineSpot;
    }

    public Area getBankArea(){ return bankArea; }

    public Area getOreArea(){ return oreArea; }

    public Ores getOre() {return ores; }

    public boolean standStill() { return !(mineSpot.equals(new Position(0,0))); }

    public Position standStillSpot(){ return mineSpot; }


}
