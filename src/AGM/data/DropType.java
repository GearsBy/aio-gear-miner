package AGM.data;

public enum DropType {

    BANKING(0),
    M1D1(1),
    M2D2(2),
    FULL_LOAD(3);


    private final int dropType;

    DropType(final int dropType){
        this.dropType = dropType;
    }

    public int dropType(){
        return dropType;
    }
}
