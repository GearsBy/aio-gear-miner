package AGM.data;

public enum Ores {

    COPPER_TIN("COPPER_TIN", "Copper ore", 1, 7453,7484,7485,7486),
    IRON("IRON", "Iron ore", 15, 7488, 7455),
    COAL("COAL", "Coal ore", 30, 7456, 7489),
    GOLD("GOLD", "Gold", 40, 7458,7491);


    private final String name, oreName;
    private final int level;
    private final int[] ids;

    private final int[] deadIDs = {7468, 7469};

    Ores(final String name, final String oreName, final int level, final int... ids){
        this.name = name;
        this.oreName = oreName;
        this.level = level;
        this.ids = ids;
    }

    public String getName() {return name;}

    public String getOreName() {return oreName; }

    public int getLevel() {return level;}

    public int[] getIds(){ return ids;}

    public int[] getDeadIds(){ return deadIDs; }
}
