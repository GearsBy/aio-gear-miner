package AGM.data;

import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

public enum Pickaxe {

    BRONZE("Bronze pickaxe", 1, 1),
    IRON("Iron pickaxe", 1, 2),
    STEEL("Steel pickaxe", 6, 3),
    BLACK("Black pickaxe", 11, 4),
    MITHRIL("Mithril pickaxe", 21, 5),
    ADAMANT("Adamant pickaxe", 31, 6),
    RUNE("Rune pickaxe", 41, 7);


    private final String name;
    private final int level, value;

    Pickaxe(final String name, final int level, final int value){
        this.name = name;
        this.level = level;
        this.value = value;
    }

    public boolean canUse(){
        return Skills.getCurrentLevel(Skill.MINING) >= level;
    }

    public boolean canHold(){
        return (Skills.getCurrentLevel(Skill.ATTACK) +1 ) >= level;
    }

    public int getValue(){
        return value;
    }

    public String getName(){
        return name;
    }

    public boolean isThisBetter(Pickaxe pick){
        return pick.getValue() > value;
    }

}
